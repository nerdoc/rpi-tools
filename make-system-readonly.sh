#!/bin/sh

# This script makes a fresh rpi system read-only
# Have a look at https://gitlab.com/nerdoc/rpi-tools for more informaion

die() {
    echo "ERROR: ${1}"
    exit 1
}

sudo apt-get update -y && sudo apt-get full-upgrade -y
sudo apt-get install -y busybox-syslogd
sudo apt-get autoremove --purge -y triggerhappy logrotate dphys-swapfile rsyslog
sudo apt-get clean -y

echo "Setting kernel booting to readonly..."
sed -i "s/deadline rootwait/deadline rootwait fastboot noswap ro/" /boot/cmdline.txt

echo "Making partiions readonly at boot time..."
# add r/o to each existing fstab sdcard mount point
sudo sed -i "s/\(^PARTUUID.*defaults[,a-z]*\)\(.*\)/\1,ro\2/" /etc/fstab

# add r/o mount points
cat << EOF | sudo tee -a /etc/fstab >/dev/null
tmpfs   /var/lib/systemd   tmpfs     mode=0755               0 0
tmpfs   /var/lib/private   tmpfs     mode=0700               0 0
tmpfs   /var/log           tmpfs     nodev,nosuid            0 0
tmpfs   /var/tmp           tmpfs     nodev,nosuid            0 0
tmpfs   /var/cache         tmpfs     nodev,nosuid            0 0
tmpfs   /tmp               tmpfs     nodev,nosuid,mode=1777  0 0
tmpfs   /var/spool         tmpfs     defaults,noatime,nosuid,nodev,noexec,mode=0755,size=64M   0  0
EOF

echo "Setting up links to temp filesystems of necessary files..."
sudo rm -rf /var/lib/dhcp /var/lib/dhcpcd5 /var/spool /etc/resolv.conf /var/lib/systemd/random-seed
sudo ln -s /tmp /var/lib/dhcp
sudo ln -s /tmp /var/lib/dhcpcd5
sudo touch /tmp/dhcpcd.resolv.conf
sudo ln -s /tmp/dhcpcd.resolv.conf /etc/resolv.conf
sudo ln -s /tmp/random-seed /var/lib/systemd/random-seed

# This is a little dangerous, as the [Service] section is not guaranteed to be at the end of the file...
echo 'ExecStartPre=/bin/echo "" >/tmp/random-seed' | sudo tee -a /lib/systemd/system/systemd-random-seed.service >/dev/null

echo "Installing rw/ro commands at login..."
if ! grep -q set_bash_prompt /etc/bash.bashrc; then
    cat << EOF |sudo tee -a /etc/bash.bashrc >/dev/null

# ----- system readonly tools -----
set_bash_prompt() {
    fs_mode=$(mount | sed -n -e "s/^\/dev\/.* on \/ .*(\(r[w|o]\).*/\1/p")    
    PS1='\[\033[01;32m\]\u@\h${fs_mode:+($fs_mode)}\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
}
alias ro='sudo mount -o remount,ro /; sudo mount -o remount,ro /boot'
alias rw='sudo mount -o remount,rw /; sudo mount -o remount,rw /boot'
export PROMPT_COMMAND=set_bash_prompt
EOF
fi

# make sure you remount disks in r/o when you logout
# FIXME: this is unsafe, as it could be locked. Rebboot always works, 
# or stopping relevant services
if [ -f /etc/bash.bash_logout ]; then
    OPT="--append"
fi
if ! grep -q "mount" /etc/bash.bash_logout; then
    echo "mount -o remount,ro /" | sudo tee $OPT /etc/bash.bash_logout >/dev/null
    echo "mount -o remount,ro /boot" | sudo tee $OPT /etc/bash.bash_logout >/dev/null
fi

echo "Finished."
echo
echo "You should reboot now before installing other software."
echo "You can use the commands 'rw' and 'ro' to switch the system into/from readonly mode when you are logged in."
