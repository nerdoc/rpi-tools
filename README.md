# RPi-Tools

This is a repository of useful tools for the Raspberry-Pi. ATM only one script.

## Description
This script is intended to turn a freshly installed RasPi OS into a readonly system, to save the SD card from corruption.
This works for many software applicances (webserver, raspotify, garage door openers etc.), but won't for some others, where you need to save data on your SD, YMMV. Don't forget you could save data on an external device (USB disk, network).

## Usage
1. Get a RasPi image and burn it onto a SD card [e.g. with RPI Imager](https://www.raspberrypi.org/software/). I recommend the headless image without desktop (Raspberry Pi OS Lite).
1. Place a file named "ssh" into the "boot" disk image, to enable SSH access
1. Boot it, ssh into that system (user "pi", default password "raspberry") and start this script here by either cloning this repo, or, if you dare, running

```bash
curl https://gitlab.com/nerdoc/rpi-tools/-/raw/main/make-system-readonly.sh | sh
```

## Roadmap
Maybe I'll add some other tools for installing software, depending on what I need.

## Example usage

* You can e.g. easily make a [Raspotify client](https://dtcooper.github.io/raspotify/) by executing

  ```bash
  curl -sL https://dtcooper.github.io/raspotify/install.sh | sh
  ```
  ...and a few moments later you can use your old stereo as a Spotify-enabled music box. Kewl.

Other ideas are welcome.

## Contributing
Issues and PRs welcome.

## Authors, Credits and acknowledgment
* https://medium.com/swlh/make-your-raspberry-pi-file-system-read-only-raspbian-buster-c558694de79
* https://www.heise.de/ratgeber/Anleitung-Raspberry-Pi-mit-schreibgeschuetztem-Linux-betreiben-6143863.html

## License
MIT License. feel free to use it.

## Project status
Alpha. Currently in use by me. Not know if at anyone else too.

